package ejercicio03;

import java.util.Scanner;

public class Ejercicio03 {

	public static void main(String[] args) {
		Scanner lector = new Scanner(System.in);
		System.out.println("Dame una letra");
		char letra = lector.nextLine().charAt(0);
		if (letra >= 'A' && letra <= 'Z') {
			System.out.println("La letra es mayuscula");
		} else {
			System.out.println("La letra es minuscula");
		}
		lector.close();
	}

}
