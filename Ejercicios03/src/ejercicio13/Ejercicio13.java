package ejercicio13;

import java.util.Scanner;

public class Ejercicio13 {

	public static void main(String[] args) {
		Scanner lector = new Scanner(System.in);

		System.out.println("Escribe una cadena");
		String s1 = lector.nextLine();
		System.out.println("Escribe otra cadena");
		String s2 = lector.nextLine();
		if (s1.equals(s2)) {
			System.out.println("Las cadenas son iguales");
		} else if (s1.equalsIgnoreCase(s2)) {
			System.out.println("Las cadenas son iguales, pero una est� en may�sculas");
		} else {
			System.out.println("Las cadenas son ditintas");
		}

		lector.close();

	}

}
