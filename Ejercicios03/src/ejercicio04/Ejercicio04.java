package ejercicio04;

import java.util.Scanner;

public class Ejercicio04 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Dame un caracter");
		char letra1 = input.nextLine().charAt(0);
		System.out.println("Dame otro caracter");
		char letra2 = input.nextLine().charAt(0);
		if (letra1 == letra2) {
			System.out.println("Son iguales");
		} else {
			System.out.println("No son iguales");
		}
		input.close();
	}

}
