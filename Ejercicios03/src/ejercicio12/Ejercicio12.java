package ejercicio12;

import java.util.Scanner;

public class Ejercicio12 {

	public static void main(String[] args) {
		Scanner lector = new Scanner(System.in);

		System.out.println("Introduce el mes en n�mero");
		int mes = lector.nextInt();
		// 1,3,5,7,8,10,12 de 31, 2 de 28, 4,6,9,11 de 30
		if (mes >= 13 || mes <= 0) {
			System.out.println(mes + " no es un mes valido");
		} else if ((mes == 4) || (mes == 6) || (mes == 9) || (mes == 11)) {
			System.out.println("El mes tiene 30 d�as");
		} else if (mes == 2) {
			System.out.println("El mes tiene 28 dias");
		} else {
			System.out.println("El mes tiene 31 dias");
		}

		lector.close();
	}

}
