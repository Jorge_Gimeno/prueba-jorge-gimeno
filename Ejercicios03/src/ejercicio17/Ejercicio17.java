package ejercicio17;

import java.util.Scanner;

public class Ejercicio17 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Dame el d�a uno");
		int dia1 = input.nextInt();
		System.out.println("Dame el mes uno");
		int mes1 = input.nextInt();
		System.out.println("Dame el a�o uno");
		int anno1 = input.nextInt();
		System.out.println("Dame el d�a dos");
		int dia2 = input.nextInt();
		System.out.println("Dame el mes dos");
		int mes2 = input.nextInt();
		System.out.println("Dame el a�o 2");
		int anno2 = input.nextInt();
		int fecha1 = dia1 + mes1 * 30 + anno1 * 360;
		int fecha2 = dia2 + mes2 * 30 + anno2 * 360;
		System.out.println("Tienen " + (fecha2 - fecha1) + " d�as de diferencia");
		input.close();
	}
}
