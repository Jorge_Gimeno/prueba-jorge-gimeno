package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {
		Scanner lector = new Scanner(System.in);
		System.out.println("Dame un numero");
		int numero = lector.nextInt();
		if (numero % 2 == 0) {
			System.out.println("El numero es par");
		} else {
			System.out.println("El numero es impar");
		}
		lector.close();

	}

}
