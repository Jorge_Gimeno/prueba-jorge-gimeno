package ejercicio02;

import java.util.Scanner;

public class Ejercicio02 {

	public static void main(String[] args) {
		Scanner lector = new Scanner(System.in);
		System.out.println("Dame un numero");
		int numero = lector.nextInt();
		if (numero % 10 == 0) {
			System.out.println("El numero es multiplo de 10");
		} else {
			System.out.println("El numero no es multiplo de 10");
		}
		lector.close();

	}

}
